<?php

/**
 * @file
 * Tests functionality of the Maintenance 200 module.
 */

/**
 * Tests functionality of the Maintenance 200 module.
 */
class Maintenance200TestCase extends DrupalWebTestCase {
  
  protected $admin_user;

  public static function getInfo() {
    return array(
      'name' => 'Maintenance 200',
      'description' => 'Ensure that the Maintenance 200 module returns the selected HTTP Status when maintenance mode is enabled.',
      'group' => 'Maintenance 200',
    );
  }

  public function setUp() {
    // Enable the Maintenance 200 module.
    parent::setUp('maintenance200');

    // Create and log in as an admin user.
    $this->admin_user = $this->drupalCreateUser(array('administer site configuration'));
    $this->drupalLogin($this->admin_user);
  }

  /**
   * Test all available maintenance mode statuses.
   */
  public function testMaintenance200Statuses() {
    $statuses = maintenance200_get_allowed_statuses();
    
    foreach ($statuses as $status_code => $status_text) {
      // Set maintenance mode and status.
      $edit = array(
        'maintenance_mode' => 1,
        'maintenance_mode_status' => $status_code,
      );
      $this->drupalPost('admin/config/development/maintenance', $edit, t('Save configuration'));

      // Verify settings.
      $this->assertTrue(variable_get('maintenance_mode', FALSE), 'Site is in maintenance mode.');
      $this->assertEqual(variable_get('maintenance_mode_status', ''), $status_code, 'Maintenance mode status is set correctly.');

      // Check anonymous user response.
      $this->drupalLogout();
      $this->drupalGet('');
      $this->assertResponse(explode(' ', $status_code)[0], format_string('Site returns a @status Status code.', array('@status' => $status_code)));

      // Log back in as admin.
      $this->drupalLogin($this->admin_user);
    }
  }

  /**
   * Test that invalid status codes are not accepted.
   */
  public function testInvalidStatusCode() {
    $edit = array(
      'maintenance_mode' => 1,
      'maintenance_mode_status' => '999 Invalid Status',
    );
    $this->drupalPost('admin/config/development/maintenance', $edit, t('Save configuration'));
    $this->assertText('Invalid maintenance mode status selected.', 'Invalid status code was rejected.');
  }
}
